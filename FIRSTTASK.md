## First Task

- Follow the Project setup and Compiles and hot-reloads for development to get the 'hello world' page on your browser at http://localhost:8080/
- Create a New Branch in git named 'hello'
- Change 'Hello World!!!' text to 'Hello [YOUR-NAME]' i.e. 'Hello Fred'
- Check that this is reflected in the browser view by refreshing the browser page
- Create a commit with this code change
