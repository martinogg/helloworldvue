# Hello World Vue

This is a test project as a guide for new developers. Vue.js is a framework for the development of interactive websites. For now, Its not important if you are unfamilar with Vue, the purpose of this
project is get set up with a project that already exists, run and test locally, make simple code
changes, then upload for others to see and use.

# Youtube Video recommendations

- Why You Should Become a Self-Taught Programmer in 2020 https://www.youtube.com/watch?v=_i1Nzi2eHzM&ab_channel=AndySterkowitz
- What is a Git Repository? [Beginner Git Tutorial] https://www.youtube.com/watch?v=A-4WltCTVms&ab_channel=GitKraken
- Vue JS Crash Course https://youtu.be/Wy9q22isx3U?t=430 (not necessary to watch the whole thing, but useful
  to get your computer set up initially)

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

## Software recommendations for set up

- Git (required)
  This is already installed on MacOS and Linux installations, you might need to install it on Windows.
  Git is the source control software that is need for projects on github.com and gitlab.com - the most common source code repistory sites.
  Check if you have Git already by typing `git --version` in to Terminal or Command Prompt

- Sourcetree (required)
  This is a Graphical Interface app for computers that really helps with visualising the work. There's a whole bunch of competing apps (Fork, GitX, GitKraken) but they all do the same thing. I'll go through
  the tutorial using this app

- Sublime (required)
  A text editor app that can show folders and files, show tabbed open text files. There are loads of
  similar apps but I'd recommend this as a quick and easy one to start with.
  I personally use VSCode for most of my code projects when I can, but for this example I'll just stick
  to a this

- VSCode (not necessary for now)
  VSCode is primarily a text editor app but has a whole bunch of plugins that add extra functionality,
  such as debugging and formatting for multiple types of programming languages and platforms.
  This can be used as an IDE (Integrated Development Environment) There's a whole load of customisations
  that can be used for different project requirements

- Chrome Browser
  If you don't already have this, I'd recommend it. Any web browser will work, but
  Nekologic staff tend to use Chrome as a standardised browser across all platforms. Having consistency
  comes in handy when it comes to things like viewing debugger options.

- Node (required)
  Node is a system that allows Javascript code to run on your computer outside of a web browser. This is
  required to set up and run vue projects on your computer.
  Check if you already have it by trying `node -v` on the Terminal or Command Prompt
  It should already be installed on mac and Linux

- NPM (required)
  NPM is Node Package Manager. This is required by vue projects to build and run on your computer.
  This is used to install 3rd party software libraries that vue depends on.
  Check if you already have it by trying `npm -v` on the Terminal or Command Prompt
  It should already bee installed on mac and Linux

### Tasks to complete

Install the required apps above

Clone the project to your local
(Cloning a project means copying the git repository to your own computer so you can get set up and running)

After you have cloned the project look at file ./FIRSTTASK.md for the task
